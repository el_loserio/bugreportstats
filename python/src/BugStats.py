'''
See LICENCE_GPL for licensing information

@author: spd
'''

from Utils import seed_random, parse_config
from RandomBugs import AttrDict, XmlDictConfig
from Column import Column
from RestrictedError import RestrictedError
import bugzilla
import random
import operator
import base64
import os
from httplib import HTTPException
import Csv

def get_description(bug):
    desc = first(bug, 'long_desc')
    if desc:
        return desc['thetext']

def get_comments(bug):
    desc = rest(bug, 'long_desc')
    if desc:
        return ' '.join(d['thetext'] for d in desc[1:])

def get_attachments(bug, comments=False):
    cmp = operator.ne if comments else operator.eq
    desc = first(bug, 'long_desc')
    attachments = all(bug, 'attachment')
    if desc and attachments:
        results = []
        for attach in attachments:
            if cmp(desc.get('attachid'), attach['attachid']):
                results += [attach['desc']]
                results += [attach['type']]
                if attach['type'].startswith('text') and not attach['ispatch']:
                    results += [base64.b64decode(attach['data']['data.value'])]
                break
        return ' '.join(results)

def get_comment_attachments(bug):
    return get_attachments(bug, True)

def first(bug, attr):
    val=bug.bug.get(attr)
    if isinstance(val, list):
        val=val[0]
    return val

def rest(bug, attr):
    val=bug.bug.get(attr)
    if isinstance(val, list):
        return val[1:]
    return []

def all(bug, attr):
    val=bug.bug.get(attr)
    if val:
        if isinstance(val, list):
            return val
        return [val]
    
columns = []
columns += [Column('ID', key='bug_id')] 
columns += [Column('Product', key='product')]
columns += [Column('Component', key='component')]
columns += [Column('Version', key='version')]
columns += [Column('Severity', key='bug_severity')]
columns += [Column('Hardware', key='rep_platform')]
columns += [Column('OS', key='op_sys')]
columns += [Column('Summary', key='short_desc')]
columns += [Column('Status', key='bug_status')]
columns += [Column('Resolution', key='resolution')]
columns += [Column('Creation Date', key='creation_ts')]
columns += [Column('Last Modification Date', key='delta_ts')]
columns += [Column('Build information')]
columns += [Column('Observed behaviour')]
columns += [Column('Expected behaviour')]
columns += [Column('Steps to reproduce')]
columns += [Column('Stack trace')]
columns += [Column('Screenshots')]
columns += [Column('Code examples')]
columns += [Column('App Code')]
columns += [Column('Error reports')]
columns += [Column('Test Cases')]
columns += [Column('Description', func=get_description)]
columns += [Column('Comments', func=get_comments)]
columns += [Column('Attachments', func=get_attachments)]
columns += [Column('Comment Attachments', func=get_comment_attachments)]
columns[-1].sep = False

def extract_bugs(out_file, existing_ids, results_file, url, restrict, search, size, max, seed):
    seed = seed_random(seed=seed)
    print 'Seed used', seed
    print 
    
    results = {}
    heading = None
    if results_file:
        for line in results_file:
            values = [w.strip('"\n\t ') for w in line.split(',')]
            if not heading:
                heading = values
            else:
                result = dict(zip(heading, values))
                results[result['ID']] = result
    
    bugz = bugzilla.Bugz(url, skip_auth=True)
    
    bugs = []
    if not search or not restrict:
        while len(bugs) < size:
            id = existing_ids.pop() if existing_ids else str(random.randint(1, max))
            if id not in [b.bug.bug_id for b in bugs]:
                try:
                    tree = bugz.get(id)
                    if tree:
                        bug = AttrDict(XmlDictConfig(tree.getroot()))
                        for k,v in restrict.items():
                            if bug.bug[k] != v:
                                raise RestrictedError
                        bugs += [bug]
                except RestrictedError:
                    pass
                except HTTPException:
                    pass
    else:
        if 'query' not in restrict:
            restrict['query'] = ''
        all_bugs = bugz.search(**restrict)
        found_ids = [ b['bugid'] for b in all_bugs if int(b['bugid']) <= max]
        ids = filter(lambda x: x in found_ids, existing_ids)
        ids += random.sample(found_ids, size - len(existing_ids))
        for id in ids:
            tree = bugz.get(id)
            bug = AttrDict(XmlDictConfig(tree.getroot()))
            bugs += [bug]
            
    bugs.sort(key=lambda b: int(b.bug.bug_id))
           
    for col in columns:
        col.write_title(out_file)
    
    for bug in bugs:
        for col in columns:
            col.write_value(out_file, bug, results)

if __name__ == '__main__':
    config = parse_config('config')
    for project in config.sections():
        seed = config.getint(project, 'sample.seed')
        base_path = os.path.join(config.get(project, 'sample.out_dir'), project + '_' + str(seed))
        file_path = base_path + '.csv'
        results_path = base_path + '_results.csv'
        existing = []
        if os.path.exists(file_path):
            for line in open(file_path, 'r'):
                id = line.split(',')[0]
                try:
                    existing += [str(int(id))]
                except ValueError:
                    pass
        
        file = open(file_path, 'w')
        if os.path.exists(results_path):
            results = open(results_path)
        url = config.get(project, 'sample.url')
        search = config.getboolean(project, 'sample.search')
        size = config.getint(project, 'sample.size')
        max = config.getint(project, 'sample.max')
        
        restrict = {}
        for k in config.options(project):
            if k.startswith('sample.restrict'):
                restrict[k.replace('sample.restrict.', '')] = config.get(project, k)
                
        extract_bugs(file, existing, results, url, restrict, search, size, max, seed)
